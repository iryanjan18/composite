@Data
@NoArgsConstructor
@Entity(name = "Role")
@Table(
  name = "role",
  uniqueConstraints = { @UniqueConstraint(name = "role_name_unique", columnNames = "name") }
)
public class Role {

  @Id
  @SequenceGenerator(name = "role_sequence", sequenceName = "role_sequence", allocationSize = 1)
  @GeneratedValue(strategy = SEQUENCE, generator = "role_sequence")
  @Column(name = "id", updatable = false)
  private Long id;

  @Enumerated(STRING)
  @Column(name = "name", nullable = false, columnDefinition = "TEXT")
  private ERole name;

  @OneToMany(
    cascade = {PERSIST, REMOVE},
    mappedBy = "role"
  )
  private List<UserRole> roles = new ArrayList<>();

  @ManyToOne
  @JoinColumn(
    name = "app_id",
    nullable = false,
    referencedColumnName = "id",
    foreignKey = @ForeignKey(name = "app_role_fk")
  )
  private App app;

  public void addRole(UserRole role) {
    if (!roles.contains(role)) {
      roles.add(role);
    }
  }

  public void removeRole(UserRole role) {
    roles.remove(role);
  }

}
