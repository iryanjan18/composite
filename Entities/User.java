@Data
@NoArgsConstructor
@Entity(name = "User")
@Table(
  name = "app_user",
  uniqueConstraints = {
    @UniqueConstraint(name = "user_email_unique", columnNames = "email"),
    @UniqueConstraint(name = "user_employee_id_unique", columnNames = "employee_id")
  }
)
public class User implements UserDetails, Serializable {

  private static final long serialVersionUID = 2309547020190971902L;

  @Id
  @Column(name = "id", updatable = false)
  private UUID id;

  @Column(name = "employee_id", nullable = false)
  private UUID employeeId;

  @Column(name = "email", nullable = false, length = 100)
  private String email;

  @Column(name = "password", nullable = false, columnDefinition = "TEXT")
  private String password;

  @Enumerated(STRING)
  @Column(name = "role", nullable = false, columnDefinition = "TEXT")
  private ERole role;

  @Column(name = "locked", nullable = false)
  private Boolean locked = false;

  @Column(name = "enabled", nullable = false)
  private Boolean enabled = false;

  @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime createdAt;

  @Column(name = "updated_at", columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime updatedAt;

  @OneToOne(
    mappedBy = "user",
    orphanRemoval = true,
    cascade = {PERSIST, MERGE, REMOVE},
    fetch = LAZY
  )
  private Token token;

  @OneToMany(
    cascade = {PERSIST, REMOVE},
    mappedBy = "user"
  )
  private List<UserRole> roles = new ArrayList<>();

  public User(UUID employeeId, String email, String password, ERole role) {
    this.employeeId = employeeId;
    this.email = email;
    this.password = password;
    this.role = role;
  }

  public void addRole(UserRole role) {
    if (!roles.contains(role)) {
      roles.add(role);
    }
  }

  public void removeRole(UserRole role) {
    roles.remove(role);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.name());
    return Collections.singleton(authority);
  }

  public UUID getId() {
    return id;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return !locked;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return enabled;
  }

}
