@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "UserRole")
@Table(name = "user_role")
public class UserRole implements Serializable {

  private static final long serialVersionUID = 1342846611307825350L;

  @EmbeddedId
  private UserRoleId id;

  @ManyToOne
  @MapsId("userId")
  @JoinColumn(
    name = "user_id",
    foreignKey = @ForeignKey(name = "user_id_fk")
  )
  private User user;

  @ManyToOne
  @MapsId("roleId")
  @JoinColumn(
    name = "role_id",
    foreignKey = @ForeignKey(name = "role_id_fk")
  )
  private Role role;

  @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime createdAt;

  public UserRole(User user, Role role, LocalDateTime createdAt) {
    this.user = user;
    this.role = role;
    this.createdAt = createdAt;
  }

  public UserRole(Role role, LocalDateTime createdAt) {
    this.role = role;
    this.createdAt = createdAt;
  }

}
