@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleId implements Serializable {

  private static final long serialVersionUID = 3209608798899457728L;

  @Column(name = "user_id")
  private UUID userId;

  @Column(name = "role_id")
  private Long roleId;

}
