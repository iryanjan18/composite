@Repository
@Transactional(readOnly = true)
public interface UserRoleRepository extends JpaRepository<UserRole, UserRoleId> {
}
